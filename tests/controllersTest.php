<?php

use Silex\WebTestCase;

class controllersTest extends WebTestCase
{

    public function testGetPerson()
    {
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/person/1');

        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('{"name": "Luke Skywalker", "height": "172", "hair_color": "blond", "birth_year": "19BBY", "homeworld": "Tatooine", "films": ["The Empire Strikes Back", "Revenge of the Sith", "Return of the Jedi", "A New Hope", "The Force Awakens"]}', $crawler->filter('body')->text());
    }

    public function testGetPeople()
    {
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/people');

        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('"name": "Luke Skywalker", "birth_year": "19BBY"', $crawler->filter('body')->text());
    }

    public function testGetPersonRouting()
    {
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/person/4');

        $this->assertTrue($client->getResponse()->isOk());
    }

    public function testGetPeopleRouting()
    {
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/people');

        $this->assertTrue($client->getResponse()->isOk());
    }

    public function testGetHomepage()
    {
        $client = $this->createClient();
        $client->followRedirects(true);
        $crawler = $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isOk());
        $this->assertContains('Star Cache', $crawler->filter('body')->text());
    }

    public function testTautology()
    {
        $this->assertTrue(true);
    }

    public function createApplication()
    {
        $app = require __DIR__.'/../src/app.php';
        require __DIR__.'/../config/dev.php';
        require __DIR__.'/../src/controllers.php';
        $app['session.test'] = true;

        return $this->app = $app;
    }
}
