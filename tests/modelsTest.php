<?php

use Silex\WebTestCase;
use Models\Person;

class modelsTest extends WebTestCase
{
    public function testModelCreation()
    {
        $person = new Person("test name","1cm", "blond", "0BBY", "test home", ['film1', 'film2']);
        $this->assertTrue(is_object($person));
    }

    public function testTautology()
    {
        $this->assertTrue(true);
    }

    public function createApplication()
    {
        $app = require __DIR__.'/../src/app.php';
        require __DIR__.'/../config/dev.php';
        require __DIR__.'/../src/controllers.php';
        $app['session.test'] = true;

        return $this->app = $app;
    }
}
