<?php
namespace SwapiConnector;

use Models\Person;

// The SwapiCache handles database access and record creation
class SwapiCache
{
    private static $db;
    private static $result;

    public static function hasPersonDetails($id)
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $result = pg_query_params($db,
                                  "SELECT * FROM people WHERE id=$1",
                                  array($id));
        if (pg_num_rows($result) > 0) {
            return true;
        }
        return false;
    }

    public static function getPerson($id)
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $result = pg_query_params($db,
                                  "SELECT * FROM people WHERE id=$1",
                                  array($id));
        $row = pg_fetch_assoc($result);
        $name = $row['name'];
        $height = $row['height'];
        $hair = $row['hair_color'];
        $birth = $row['birth_year'];
        $homeworld = $row['homeworld'];
        $dbFilms = $row['films'];
        $films = explode(",", str_replace(["{", "}", '"'], "", $dbFilms));
        return new Person($name, $height, $hair, $birth, $homeworld, $films);
    }

    public static function storePerson($id, $person)
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $pgName = pg_escape_string($person->name());
        $pgHeight = pg_escape_string($person->height());
        $pgHair = pg_escape_string($person->hairColor());
        $pgBirth = pg_escape_string($person->birthYear());
        $pgHomeworld = pg_escape_string($person->homeworld());
        $quoteField = function($string) {
            $pgString = pg_escape_string($string);
            return "\"$pgString\"";
        };
        $pgFilms = implode(",", array_map($quoteField, $person->films()));
        $pgFilms = "{".$pgFilms."}";
        $query = "INSERT INTO people VALUES ('$id', '$pgName', '$pgHeight', '$pgHair', '$pgBirth', '$pgHomeworld', '$pgFilms')";
        pg_query($db, $query);
    }

    public static function hasHomeworld($homworldUrl)
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $result = pg_query_params($db,
                                  "SELECT url, name FROM homeworlds WHERE url=$1",
                                  array($homworldUrl));
        if (pg_num_rows($result) > 0) {
            return true;
        }
        return false;
    }

    public static function getHomeworld($homworldUrl)
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $result = pg_query_params($db,
                                  "SELECT url, name FROM homeworlds WHERE url=$1",
                                  array($homworldUrl));
        $row = pg_fetch_assoc($result);
        return $row['name'];
    }

    public static function storeHomeworld($homworldUrl, $homeworld)
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $pgHomeworld = pg_escape_string($homeworld);
        $pgUrl = pg_escape_string($homworldUrl);
        $query = "INSERT INTO homeworlds VALUES ('$pgUrl', '$pgHomeworld')";
        pg_query($db, $query);
    }


    public static function hasFilm($filmUrl)
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $result = pg_query_params($db,
                                  "SELECT id, url, name FROM films WHERE url=$1",
                                  array($filmUrl));
        if (pg_num_rows($result) > 0) {
            return true;
        }
        return false;
    }

    public static function getFilm($filmUrl)
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $result = pg_query_params($db,
                                  "SELECT id, url, name FROM films WHERE url=$1",
                                  array($filmUrl));
        $row = pg_fetch_assoc($result);
        return $row['name'];
    }

    public static function storeFilm($filmUrl, $film)
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $pgTitle = pg_escape_string($film);
        $pgUrl = pg_escape_string($filmUrl);
        $query = "INSERT INTO films VALUES (1, '$pgUrl', '$pgTitle')";
        pg_query($db, $query);
    }

    public static function hasPersonListing()
    {
        $db = pg_connect(getenv('DATABASE_URL'));
        $result = pg_query($db, "SELECT * FROM listing");
        if (pg_num_rows($result) > 0) {
          return true;
        }
        return false;
    }

    public static function getPersonListing()
    {
        $output = "[";
        $db = pg_connect(getenv('DATABASE_URL'));
        $result = pg_query($db, "SELECT * FROM listing");
        $results = pg_fetch_all($result);
        forEach($results as $row) {
            if ($output !== "[") {
                $output .= ",";
            }
            $output .= "{";
            $output .= '"id": "'.$row['id'].'", ';
            $output .= '"name": "'.$row['name'].'", ';
            $output .= '"birth_year": "'.$row['year'].'"';
            $output .= "}";
        }
        $output .= "]";
        return $output;
    }


    public static function storePersonListing($person)
    {
      $db = pg_connect(getenv('DATABASE_URL'));
      $pgName = pg_escape_string($person->name());
      $pgYear = pg_escape_string($person->birthYear());
      $pgId = $person->id();
      $query = "INSERT INTO listing VALUES ($pgId, '$pgName', '$pgYear')";
      pg_query($db, $query);
    }



}