<?php
namespace SwapiConnector;

use Models\Person;

// The SwapiConnector class mediated between cached resources and accessing the api
class SwapiConnector
{
    public static function getPersonData($id)
    {
        if (SwapiCache::hasPersonDetails($id)){
            $person = SwapiCache::getPerson($id);
        } else {
            $person = SwapiApi::getPerson($id);
        }
        return $person->detailJsonString();
    }

    public static function homeworldLookup($homworldUrl)
    {
        if (SwapiCache::hasHomeworld($homworldUrl)){
            $homeworld = SwapiCache::getHomeworld($homworldUrl);
        } else {
            $homeworld = SwapiApi::getHomeworld($homworldUrl);
        }
        return $homeworld;
    }

    public static function filmsLookup($filmUrls)
    {
        $films = array();
        foreach ($filmUrls as $filmUrl) {
            $filmTitle = SwapiConnector::getFilmTitle($filmUrl);
            array_push($films, $filmTitle);
        }
        return $films;
    }

    public static function getFilmTitle($filmUrl)
    {
        if (SwapiCache::hasFilm($filmUrl)){
            $film = SwapiCache::getFilm($filmUrl);
        } else {
            $film = SwapiApi::getFilm($filmUrl);
        }
        return $film;
    }

    public static function getPeopleData()
    {
        if (SwapiCache::hasPersonListing()){
            $output = SwapiCache::getPersonListing();
        } else {
            $output = SwapiApi::getPersonListing();
        }
        return $output;
    }
}

?>
