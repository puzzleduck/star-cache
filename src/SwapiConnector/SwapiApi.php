<?php
namespace SwapiConnector;

use Models\Person;

// The SwapiApi requests data directly from the SWAPI server and delegates storage to SwapiCache
class SwapiApi
{

    public static function getPerson($id)
    {
        $apiPage = "https://swapi.co/api/people/$id";
        $apiData = file_get_contents($apiPage);
        $person = json_decode($apiData);
        $name = $person->{'name'};
        $height = $person->{'height'};
        $hair = $person->{'hair_color'};
        $birth = $person->{'birth_year'};
        $homeworld = SwapiConnector::homeworldLookup($person->{'homeworld'});
        $films = SwapiConnector::filmsLookup($person->{'films'});
        $newPerson = new Person($name,
                                $height,
                                $hair,
                                $birth,
                                $homeworld,
                                $films);

        SwapiCache::storePerson($id, $newPerson);
        return $newPerson;
    }

    public static function getHomeworld($homworldUrl)
    {
        $apiData = file_get_contents($homworldUrl);
        $homeworldData = json_decode($apiData);
        $homeworld = $homeworldData->{'name'};

        SwapiCache::storeHomeworld($homworldUrl, $homeworld);
        return $homeworld;
    }

    public static function getFilm($filmUrl)
    {
        $apiData = file_get_contents($filmUrl);
        $filmData = json_decode($apiData);
        $film = $filmData->{'title'};

        SwapiCache::storeFilm($filmUrl, $film);
        return $film;
    }

    private static function processPeopleJson($data)
    {
        $jsonData = json_decode($data);
        $output = "";
        foreach ($jsonData->results as $person) {
            if ($output !== "") {
                $output .= ",";
            }
            $newPerson = new Person($person->{'name'},
                                    $person->{'height'},
                                    $person->{'hair_color'},
                                    $person->{'birth_year'},
                                    $person->{'homeworld'},
                                    $person->{'films'});
            SwapiCache::storePersonListing($newPerson);
            $output .= $newPerson->listingJsonString();
        }
        return $output;
    }


    public static function getPersonListing()
    {
        $output = "[";
        $nextPage = 'https://swapi.co/api/people?page=1';
        while ($nextPage) {
            if ($output !== "[") {
                $output .= ",";
            }
            $apiData = file_get_contents($nextPage);
            $apiJson = json_decode($apiData);
            $output .= SwapiApi::processPeopleJson($apiData);
            $nextPage = $apiJson->next;
        }
        $output .= "]";
        return $output;
    }

}
