<?php

namespace Models;

// Person is a datatype for handling SWAPI data in Star Cache
class Person
{
    private static $nextId = 1;
    private $id = 0;
    private $name = "";
    private $height = "";
    private $hairColor = "";
    private $birthYear = "";
    private $homeworld = "";
    private $films = [];

    // Provide a string representation of the details response
    public function detailJsonString()
    {
        $filmsJson = "[";
        foreach ($this->films as $film) {
            if ($filmsJson != "["){
                $filmsJson .= ", ";
            }
            $filmsJson .= '"'.$film.'"';
        }
        $filmsJson .= "]";
        return '{"name": "'.$this->name
               .'", "height": "'.$this->height
               .'", "hair_color": "'.$this->hairColor
               .'", "birth_year": "'.$this->birthYear
               .'", "homeworld": "'.$this->homeworld
               .'", "films": '.$filmsJson.'}';
    }

    // Provide a string representation of the short response
    public function listingJsonString()
    {
        return '{"id": "'.$this->id
               .'", "name": "'.$this->name
               .'", "birth_year": "'
               .$this->birthYear.'"}';
    }

    // Provide a sample of production data
    public static function getSamplePeopleData()
    {
        return '{"results": [{"name": "Luke Skywalker", '
               .'"height": "172", '
               .'"hair_color": "blond", '
               .'"birth_year": "19BBY", '
               .'"homeworld": "homeworld1",  '
               .'"films": ["film1", "film2"]}]}';
    }

    // Provide a sample of production data
    public static function getSamplePersonData()
    {
        return '{"name": "Luke Skywalker", '
               .'"height": "172", "hair_color": '
               .'"blond", "birth_year": "19BBY", '
               .'"homeworld": "homeworld1",  '
               .'"films": ["film1", "film2"]}';
    }

    // Initialize person and record unique id
    public function __construct($name,
                                $height,
                                $hairColor,
                                $birthYear,
                                $homeworld,
                                $films)
    {
        $this->id = $this::$nextId;
        $this::$nextId = $this::$nextId + 1;
        $this->name = $name;
        $this->height = $height;
        $this->hairColor = $hairColor;
        $this->birthYear = $birthYear;
        $this->homeworld = $homeworld;
        $this->films = $films;
    }

    public function name()
    {
        return $this->name;
    }

    public function height()
    {
        return $this->height;
    }

    public function hairColor()
    {
        return $this->hairColor;
    }

    public function birthYear()
    {
        return $this->birthYear;
    }

    public function homeworld()
    {
        return $this->homeworld;
    }

    public function films()
    {
        return $this->films;
    }

    public function id()
    {
        return $this->id;
    }
}

?>
