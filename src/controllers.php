<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Models\Person;
use SwapiConnector\SwapiConnector;

// Return some indicator of what the site is
$app->get('/', function(){
    return "Star Cache - SWAPI";
});

// Return a list of all people in our list
$app->get('/people', function(){
    $apiData = SwapiConnector::getPeopleData();
    return $apiData;
});

// Return the details of a specific individual
$app->get('/person/{id}', function($id){
    $apiData = SwapiConnector::getPersonData($id);
    return $apiData;
});

// Handle errors
$app->error(function (\Exception $e, Request $request, $code) use ($app) {
      if ($app['debug']) {
          return;
      }
      $templates = array(
          'errors/'.$code.'.html.twig',
          'errors/'.substr($code, 0, 2).'x.html.twig',
          'errors/'.substr($code, 0, 1).'xx.html.twig',
          'errors/default.html.twig',
      );
      return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
